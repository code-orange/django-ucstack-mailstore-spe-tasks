from django.core.management.base import BaseCommand

from django_ucstack_mailstore_spe_tasks.django_ucstack_mailstore_spe_tasks.tasks import *


class Command(BaseCommand):
    help = "Run task: ucstack_mailstore_spe_smtp_config"

    def handle(self, *args, **options):
        ucstack_mailstore_spe_smtp_config()
