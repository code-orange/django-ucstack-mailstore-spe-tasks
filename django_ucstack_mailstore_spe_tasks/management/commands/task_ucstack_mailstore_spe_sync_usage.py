from django.core.management.base import BaseCommand

from django_ucstack_mailstore_spe_tasks.django_ucstack_mailstore_spe_tasks.tasks import (
    ucstack_mailstore_spe_sync_usage,
)


class Command(BaseCommand):
    def handle(self, *args, **options):
        ucstack_mailstore_spe_sync_usage()
