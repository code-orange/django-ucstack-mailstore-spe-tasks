from time import sleep

from celery import shared_task

from django_directory_backend_client.django_directory_backend_client.func import *
from django_pwmgr_client.django_pwmgr_client.func import *
from django_ucstack_mailstore_spe_client.django_ucstack_mailstore_spe_client.func import *
from django_ucstack_mailstore_spe_models.django_ucstack_mailstore_spe_models.models import (
    UcMailstoreUsage,
)
from django_ucstack_models.django_ucstack_models.models import *


@shared_task(name="ucstack_mailstore_spe_admin_access_sync")
def ucstack_mailstore_spe_admin_access_sync():
    for tenant in UcTenantSettings.objects.filter(mailstore_enabled=True):
        directory_tenant = directory_backend_get_tenant(
            tenant.customer.id, tenant.customer.org_tag
        )

        instance_config = post_to_mailstore_spe_api(
            url="/invoke/GetInstanceConfiguration",
            data={
                "instanceID": tenant.mailstore_id,
            },
        )

        instance_id = instance_config["result"]["instanceID"]

        # Enable ArchiveAdmin access
        archive_admin = post_to_mailstore_spe_api(
            url="/invoke/SetArchiveAdminEnabled",
            data={
                "instanceID": instance_id,
                "enabled": "true",
            },
        )

        # Fetch SuperAdmin from Directory Backend
        super_admin = directory_backend_get_user(
            tenant.customer.id,
            directory_tenant["super_admin"],
        )

        super_admin_cred = pwmgr_get_single_credentials(
            settings.MDAT_ROOT_CUSTOMER_ID, super_admin["codeorange-pwmgr-id"][0]
        )
        super_admin_pwd = super_admin_cred["password"]

        directory_service_data = {
            "type": "activeDirectory",
            "serverName": "192.168.127.1",
            "protocol": "LDAP",
            "authenticationType": "standardAuthentication",
            "userName": directory_tenant["super_admin_upn"],
            "password": super_admin_pwd,
            "baseDN": "OU="
            + tenant.customer.org_tag
            + ",OU=Hosting,DC=intra,DC=srvfarm,DC=net",
            "microsoftExchangeUsersOnly": False,
            "enabledUsersOnly": False,
            "usersVisibleInAddressListsOnly": False,
            "groups": None,
            "userNameFormat": "samAccountName",
            "authenticationProtocol": "networkAuthentication",
            "openIdDiscoveryUrl": None,
            "openIdClientID": None,
            "openIdRedirectUrl": None,
            "openIdForceAdfsLogin": False,
            "deleteUsers": True,
        }

        directory_services = post_to_mailstore_spe_api(
            url="/invoke/SetDirectoryServicesConfiguration",
            data={
                "instanceID": instance_id,
                "config": json.dumps(directory_service_data),
            },
        )

        directory_services_sync = post_to_mailstore_spe_api(
            url="/invoke/SyncUsersWithDirectoryServices",
            data={
                "instanceID": instance_id,
                "dryRun": "false",
            },
        )

        mailstore_admin = post_to_mailstore_spe_api(
            url="/invoke/SetUserPrivileges",
            data={
                "instanceID": instance_id,
                "userName": super_admin["sAMAccountName"][0].lower(),
                "privileges": "admin",
            },
        )

    return


@shared_task(name="ucstack_mailstore_spe_optimize_indexing")
def ucstack_mailstore_spe_optimize_indexing():
    index_extensions = (
        "doc",
        "dot",
        "wbk",
        "docx",
        "docm",
        "dotx",
        "dotm",
        "docb",
        "xls",
        "xlt",
        "xlm",
        "xlsx",
        "xlsm",
        "xltx",
        "xltm",
        "xslb",
        "xla",
        "xlam",
        "xll",
        "xlw",
        "ppt",
        "pot",
        "pps",
        "pptx",
        "pptm",
        "potx",
        "potm",
        "ppam",
        "ppsx",
        "ppsm",
        "sldx",
        "sldm",
        "pub",
        "xps",
        "oxps",
        "pdf",
        "wpd",
        "wps",
        "rtf",
        "txt",
        "csv",
        "sdw",
        "sgl",
        "vor",
        "xml",
        "uot",
        "uof",
        "odt",
        "ott",
        "ods",
        "ots",
        "odp",
        "otp",
        "odg",
        "html",
        "xhtml",
        "phtml",
    )

    for tenant in UcTenantSettings.objects.filter(mailstore_enabled=True):
        instance_config = post_to_mailstore_spe_api(
            url="/invoke/GetInstanceConfiguration",
            data={
                "instanceID": tenant.mailstore_id,
            },
        )

        instance_id = instance_config["result"]["instanceID"]

        print("INSTANCE: " + instance_id)

        index_config_set = {"attachmentExtensions": " ".join(index_extensions)}

        index_config = post_to_mailstore_spe_api(
            url="/invoke/SetIndexConfiguration ",
            data={
                "instanceID": tenant.mailstore_id,
                "config": json.dumps(index_config_set),
            },
        )

    return


@shared_task(name="ucstack_mailstore_spe_create_instance")
def ucstack_mailstore_spe_create_instance():
    for tenant in UcTenantSettings.objects.filter(
        mailstore_enabled=True, customer__org_tag__isnull=False
    ):
        instance_id = tenant.mailstore_id

        print("INSTANCE: " + instance_id)

        instance_config = post_to_mailstore_spe_api(
            url="/invoke/GetInstanceConfiguration",
            data={
                "instanceID": instance_id,
            },
        )

        if "statusCode" in instance_config:
            if not instance_config["statusCode"] == "failed":
                continue

        print("Missing Instance: " + instance_id)

        instance_config = post_to_mailstore_spe_api(
            url="/invoke/GetInstanceConfiguration",
            data={
                "instanceID": instance_id,
            },
        )

        instance_create_config = {
            "alias": None,
            "baseDirectory": "C:\\ARCHIVE\\" + instance_id,
            "debugLogEnabled": False,
            "displayName": instance_id,
            "imapServerConnectionLogEnabled": False,
            "instanceHost": "swde5729.intra.srvfarm.net",
            "instanceID": instance_id,
            "startMode": "automatic",
            "vssWriterEnabled": True,
            "vssWriterExcludeIndexes": False,
        }

        instance_create = post_to_mailstore_spe_api(
            url="/invoke/CreateInstance",
            data={
                "config": json.dumps(instance_create_config),
            },
        )

    return


@shared_task(name="ucstack_mailstore_spe_smtp_config")
def ucstack_mailstore_spe_smtp_config():
    for tenant in UcTenantSettings.objects.filter(mailstore_enabled=True):
        instance_id = tenant.mailstore_id

        print("INSTANCE: " + instance_id)

        smtp_config = {
            "hostname": "mail.example.com",
            "port": 587,
            "protocol": "SMTP-TLS",
            "ignoreSslPolicyErrors": False,
            "authenticationRequired": True,
            "username": "sending.user@example.com",
            "password": "userpassword",
            "fromDisplayName": "Sending User",
            "fromEmailAddress": "sending.user@example.com",
            "recipientEmailAddress": "administrator@example.com",
        }

        # instance_smtp = post_to_mailstore_spe_api(
        #     url='/invoke/SetSmtpSettings',
        #     data={
        #         'config': json.dumps(smtp_config),
        #     }
        # )

    return


@shared_task(name="ucstack_mailstore_spe_remove_integrated_users")
def ucstack_mailstore_spe_remove_integrated_users():
    for tenant in UcTenantSettings.objects.filter(mailstore_enabled=True):
        print("Instance: " + tenant.mailstore_id)

        instance_config = post_to_mailstore_spe_api(
            url="/invoke/GetInstanceConfiguration",
            data={
                "instanceID": tenant.mailstore_id,
            },
        )

        instance_users = post_to_mailstore_spe_api(
            url="/invoke/GetUsers",
            data={
                "instanceID": tenant.mailstore_id,
            },
        )

        for instance_user in instance_users["result"]:
            if instance_user["userName"] == "$archiveadmin":
                continue

            instance_user_details = post_to_mailstore_spe_api(
                url="/invoke/GetUserInfo",
                data={
                    "instanceID": tenant.mailstore_id,
                    "userName": instance_user["userName"],
                },
            )

            if instance_user_details["result"]["authentication"] == "integrated":
                print(
                    "Instance: "
                    + tenant.mailstore_id
                    + " - User: "
                    + instance_user["userName"]
                )
                instance_user_details = post_to_mailstore_spe_api(
                    url="/invoke/DeleteUser",
                    data={
                        "instanceID": tenant.mailstore_id,
                        "userName": instance_user["userName"],
                    },
                )

    return


@shared_task(name="ucstack_mailstore_spe_sync_contracts")
def ucstack_mailstore_spe_sync_contracts():
    pass


@shared_task(name="ucstack_mailstore_spe_sync_usage")
def ucstack_mailstore_spe_sync_usage():
    licenses = post_to_mailstore_spe_api(url="/invoke/CreateLicenseRequest")

    if "result" not in licenses:
        return

    if "instanceData" not in licenses["result"]:
        return

    for tenant in UcTenantSettings.objects.filter(mailstore_enabled=True):
        print("Instance: " + tenant.mailstore_id)

        try:
            mailstore_usage = UcMailstoreUsage.objects.get(
                customer_id=tenant.customer_id
            )
        except UcMailstoreUsage.DoesNotExist:
            mailstore_usage = UcMailstoreUsage(customer_id=tenant.customer_id)
            mailstore_usage.save()

        for license_instance in licenses["result"]["instanceData"]:
            if license_instance["id"] == tenant.mailstore_id:
                mailstore_usage.total_licenses_reported = license_instance[
                    "usedLicenseCount"
                ]
                mailstore_usage.save()

        instance_archives_job = post_to_mailstore_spe_api(
            url="/invoke/GetFolderStatistics", data={"instanceID": tenant.mailstore_id}
        )

        token = instance_archives_job["token"]
        status_version = instance_archives_job["statusVersion"]

        while True:
            instance_archives_job_status = post_to_mailstore_spe_api(
                url="/get-status",
                data={
                    "token": token,
                    "lastKnownStatusVersion": status_version,
                    "millisecondsTimeout": 5000,
                },
            )

            if instance_archives_job_status["statusCode"] == "failed":
                break

            if instance_archives_job_status["statusCode"] == "succeeded":
                total_space_bytes = 0

                for archive in instance_archives_job_status["result"]:
                    total_space_bytes += archive["size"]

                mailstore_usage.total_space_bytes = total_space_bytes
                mailstore_usage.save()

                break

            sleep(5)

    return
